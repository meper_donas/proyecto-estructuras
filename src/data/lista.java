package data;

public class lista<T> {

    private Nodo head;
    private Nodo tail;

    class Nodo {

        T key;
        Nodo next;

        public Nodo() {
        }

        public Nodo(T key, Nodo next) {
            this.key = key;
            this.next = next;
        }
    }

    public lista() {
        this.head = null;
        this.tail = null;
    }

    //insertar nodo en cola
    public void Enqueue(T key) {
        Nodo node = new Nodo(key, null);
        if (this.head == null) {
            this.head = node;
        } else {
            this.tail.next = node;
        }
        this.tail = node;
    }

    // remover nodo de cola
    public void Dequeue() {
        Nodo node = head;
        if (this.head != null) {
            this.head = node.next;
        }
    }

    //Consultar dato en cola
    public T Peek() {
        if (this.head==null) {
            throw new java.util.EmptyStackException();
        }
        //System.out.println(this.head.key.toString());   
        return this.head.key;
    }

    // insertar nodo en pila
    public void Push(T key) {
        Nodo node = new Nodo(key, null);
        if (this.head == null) {
            this.head = node;
        } else {
            node.next = head;
            this.head = node;
        }
    }

    public void Imprimir() {
        if (this.head == null) {
            System.out.println("Lista vacia!!");
            return;
        }
        Nodo p = this.head;
        while (p != null) {
            System.out.print(p.key + " ");
            p = p.next;
        }
    }

    public boolean isEmpty() {
        if (this.head == null) {
            return true;
        } else {
            return false;
        }
    }  

    @Override
    public String toString() {
        return "lista{" + "head=" + head + ", tail=" + tail + '}';
    }
    

}
