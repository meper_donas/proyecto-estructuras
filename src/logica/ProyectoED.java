package logica;

import GUI.*;
import java.util.*;
import data.*;
import logica.AVL.*;

public class ProyectoED {

    public static void main(String[] args) {
        new MenuPrincipal().setVisible(true);        
        Scanner leer = new Scanner(System.in);
        //array para guardar el cliente y conductor creado
        //ArrayList<conductor> conductores = new ArrayList<>();
        //arbol para prueba de datos
        Node rootPD= null; // PD: prueba datos
        AVL arbolPruebas = new AVL();        
        
        //arboles para el inicio de sesión
        AVL arbolClientes = new AVL();

        cliente c1 = new cliente("andres", "torres", 1245, 5432);
        cliente c2 = new cliente("julian", "carrillo", 2434, 2344);
        cliente c3 = new cliente("camilo", "tulua", 1039, 9876);
        cliente c4 = new cliente("juan", "ortegon", 2001, 5235);
        cliente c5 = new cliente("juan", "torres", 2222, 5235);

        HashMap<String, cliente> clientes = new HashMap<>();
        clientes.put(String.valueOf(c1.getDocumento()), c1);
        clientes.put(String.valueOf(c2.getDocumento()), c2);
        clientes.put(String.valueOf(c3.getDocumento()), c3);
        clientes.put(String.valueOf(c3.getDocumento()), c4);
        clientes.put(String.valueOf(c5.getDocumento()), c5);       

        Node rootCL = null;
        rootCL = arbolClientes.insert(rootCL, c1.getDocumento());
        rootCL = arbolClientes.insert(rootCL, c2.getDocumento());
        rootCL = arbolClientes.insert(rootCL, c3.getDocumento());
        rootCL = arbolClientes.insert(rootCL, c4.getDocumento());
        rootCL = arbolClientes.insert(rootCL, c5.getDocumento());

        AVL arbolConductores = new AVL();

        conductor co1 = new conductor("pipe", "ruiz", 6543, 45);
        conductor co2 = new conductor("andres", "moreno", 970, 27);
        conductor co3 = new conductor("paola", "duarte", 4130, 78);
        conductor co4 = new conductor("paco", "ramirez", 377, 22);

        Node rootCO = null;
        rootCO = arbolConductores.insert(rootCO, co1.getDocumento());
        rootCO = arbolConductores.insert(rootCO, co2.getDocumento());
        rootCO = arbolConductores.insert(rootCO, co3.getDocumento());
        rootCO = arbolConductores.insert(rootCO, co4.getDocumento());

        //colas y listas de las rutas
        lista cRuta1 = new lista();
        ArrayList<cliente> alRuta1 = new ArrayList<>();
        alRuta1.add(c1);
        alRuta1.add(c2);
        alRuta1.add(c3);        
        lista cRuta2 = new lista();
        ArrayList<cliente> alRuta2 = new ArrayList<>();
        alRuta2.add(c3);
        alRuta2.add(c5);
        lista cRuta3 = new lista();
        ArrayList<cliente> alRuta3 = new ArrayList<>();
        alRuta3.add(c2);
        lista cRuta4 = new lista();
        ArrayList<cliente> alRuta4 = new ArrayList<>();

        lista Conductores = new lista();
        Conductores.Enqueue(co4);

        System.out.println("TRASPORTO");
        boolean salida = false;
        while (salida != true) {
            System.out.println("Inicio");
            System.out.println("1. Registro");
            System.out.println("2. Iniciar Sesión");
            System.out.println("3. Pruebas de datos");
            int opcion = leer.nextInt();
            switch (opcion) {
                case 1:
                    System.out.println("Elija una opción");
                    System.out.println("1. Cliente");
                    System.out.println("2. Conductor");
                    int opcion1 = leer.nextInt();
                    switch (opcion1) {
                        case 1:
                            System.out.println("Cliente");
                            System.out.print("Ingrese su nombre: ");
                            String nombreCliente = leer.next();
                            System.out.print("Ingrese su apellido: ");
                            String apellidoCliente = leer.next();
                            System.out.print("Ingrese su documento: ");
                            long documentoCliente = leer.nextLong();
                            System.out.print("Ingrese su telefono: ");
                            short telefonoCliente = leer.nextShort();
                            System.out.println("");
                            cliente nuevoCliente = new cliente(nombreCliente, apellidoCliente, documentoCliente, telefonoCliente);
                            String valorAString = String.valueOf(documentoCliente);
                            clientes.put(valorAString, nuevoCliente);
                            rootCL = arbolClientes.insert(rootCL, nuevoCliente.getDocumento());
                            arbolClientes.orden(rootCL);
                            break;
                        case 2:
                            System.out.println("Conductor");
                            System.out.print("Ingrese su nombre: ");
                            String nombreConductor = leer.next();
                            System.out.print("Ingrese su apellido: ");
                            String apellidoConductor = leer.next();
                            System.out.print("Ingrese su documento: ");
                            long documentoConductor = leer.nextLong();
                            System.out.print("Ingrese su edad: ");
                            short edadConductor = leer.nextShort();
                            if (edadConductor >= 18) {
                                conductor nuevoConductor = new conductor(nombreConductor, apellidoConductor,
                                        documentoConductor, edadConductor);
                                Conductores.Enqueue(nuevoConductor);
                                rootCO = arbolConductores.insert(rootCO, nuevoConductor.getDocumento());
                                arbolConductores.orden(rootCO);
                            } else {
                                System.out.println("No tiene la edad permitida para conducir");
                            }
                            break;
                    }
                case 2:
                    System.out.println("Ingrese su documento");
                    int doc = leer.nextInt();
                    boolean volver = false;
                    // Menú para los clientes
                    if (arbolClientes.contains(rootCL, doc)) {
                        while (volver != true) {
                            System.out.println("Menú Clientes");
                            System.out.println("1. Pedir viaje");
                            System.out.println("2. Cancelar viaje");
                            System.out.println("3. Volver");
                            int opcion2 = leer.nextInt();
                            switch (opcion2) {
                                case 1:
                                    System.out.println("Cual es su destino?");
                                    System.out.println("1. Portoalegre");
                                    System.out.println("2. Parque Colina");
                                    System.out.println("3. Mazuren");
                                    System.out.println("4. San Rafael");
                                    System.out.println("5. Volver");
                                    int opcion3 = leer.nextInt();
                                    switch (opcion3) {
                                        case 1:
                                            System.out.println("Destino: Portoalegre");                                           
                                            if (clientes.containsKey(String.valueOf(doc))) {
                                                cliente nuevo = clientes.get(String.valueOf(doc));
                                                cRuta1.Enqueue(nuevo);
                                                System.out.println("Añadido a la cola.");
                                                while (!(cRuta1.isEmpty())) {
                                                    alRuta1.add((cliente) cRuta1.Peek());
                                                    cRuta1.Dequeue();
                                                }
                                                if (alRuta1.size() == 4) {
                                                    if (Conductores.isEmpty()) {
                                                        System.out.println("No hay conductores disponibles. Espere a que uno se encuentre disponible");
                                                    } else {
                                                        System.out.println("Pasajeros listos. Su conductor será:");
                                                        System.out.println(Conductores.Peek());
                                                    }
                                                } else {
                                                    System.out.println("Si no hay 4 pasajeros listos tendrá que esperar en la cola");
                                                }
                                            }
                                            break;
                                        case 2:
                                            System.out.println("Destino: Parque Colina");                                            
                                            if (clientes.containsKey(String.valueOf(doc))) {
                                                cliente nuevo = clientes.get(String.valueOf(doc));
                                                cRuta2.Enqueue(nuevo);
                                                System.out.println("Añadido a la cola.");
                                                while (!(cRuta2.isEmpty())) {
                                                    alRuta2.add((cliente) cRuta2.Peek());
                                                    cRuta2.Dequeue();
                                                }
                                                if (alRuta2.size() == 4) {
                                                    if (Conductores.isEmpty()) {
                                                        System.out.println("No hay conductores disponibles. Espere a que uno se encuentre disponible");
                                                    } else {
                                                        System.out.println("Pasajeros listos. Su conductor será:");
                                                        System.out.println(Conductores.Peek());
                                                    }
                                                } else {
                                                    System.out.println("Si no hay 4 pasajeros listos tendrá que esperar en la cola");
                                                }
                                            }
                                            break;
                                        case 3:
                                            System.out.println("Destino: Mazuren");                                            
                                            if (clientes.containsKey(String.valueOf(doc))) {
                                                cliente nuevo = clientes.get(String.valueOf(doc));
                                                cRuta3.Enqueue(nuevo);
                                                System.out.println("Añadido a la cola.");
                                                while (!(cRuta3.isEmpty())) {
                                                    alRuta3.add((cliente) cRuta3.Peek());
                                                    cRuta3.Dequeue();
                                                }
                                                if (alRuta3.size() == 4) {
                                                    if (Conductores.isEmpty()) {
                                                        System.out.println("No hay conductores disponibles. Espere a que uno se encuentre disponible");
                                                    } else {
                                                        System.out.println("Pasajeros listos. Su conductor será:");
                                                        System.out.println(Conductores.Peek());
                                                    }
                                                } else {
                                                    System.out.println("Si no hay 4 pasajeros listos tendrá que esperar en la cola");
                                                }
                                            }
                                            break;
                                        case 4:
                                            System.out.println("Destino: San Rafael");
                                            if (clientes.containsKey(String.valueOf(doc))) {                      
                                                cliente nuevo = clientes.get(String.valueOf(doc));
                                                System.out.println(nuevo);
                                                System.out.println("Añadido a la cola.");
                                                cRuta4.Enqueue(nuevo);
                                                while (!(cRuta4.isEmpty())) {
                                                    alRuta4.add((cliente) cRuta4.Peek());
                                                    cRuta4.Dequeue();
                                                }                                                
                                                if (alRuta4.size() == 4) {
                                                    if (Conductores.isEmpty()) {
                                                        System.out.println("No hay conductores disponibles. Espere a que uno se encuentre disponible");
                                                    } else {
                                                        System.out.println("Pasajeros listos. Su conductor será:");
                                                        System.out.println(Conductores.Peek());
                                                    }
                                                } else {
                                                    System.out.println("Si no hay 4 pasajeros listos tendrá que esperar en la cola");
                                                }
                                            }
                                            break;
                                        case 5:
                                            volver = true;
                                            break;
                                    }
                                    break;
                                case 2:
                                    System.out.println("Seguro desea cancelar su viaje?(si/no)");                                                                                                                                             
                                    String opcion4 = leer.next();
                                    if ("si".equals(opcion4)) {
                                        for (int i = 0; i < alRuta1.size(); i++) {
                                            if (alRuta1.get(i).equals(clientes.get(String.valueOf(doc)))) {                                                
                                                alRuta1.remove(i);
                                                System.out.println("Viaje a Portoalegre cancelado.");
                                            }
                                        }
                                        for (int i = 0; i < alRuta2.size(); i++) {
                                            if (alRuta2.get(i).equals(clientes.get(String.valueOf(doc)))) {                                                
                                                alRuta2.remove(i);
                                                System.out.println("Viaje a Parque Colina cancelado.");
                                            }                                            
                                        }
                                        for (int i = 0; i < alRuta3.size(); i++) {
                                            if (alRuta3.get(i).equals(clientes.get(String.valueOf(doc)))) {
                                                alRuta3.remove(i);
                                                System.out.println("Viaje a Mazuren cancelado.");
                                            }
                                        }
                                        for (int i = 0; i < alRuta4.size(); i++) {
                                            if (alRuta4.get(i).equals(clientes.get(String.valueOf(doc)))) {
                                                alRuta4.remove(i);
                                                System.out.println("Viaje a San Rafael cancelado.");
                                            }
                                        }
                                    } else {
                                        volver = true;
                                    }
                                    break;
                                case 3:
                                    volver = true;
                                    break;
                            }
                        }
                    } else if (arbolConductores.contains(rootCO, doc)) {
                        System.out.println("Menú Conductores");
                        System.out.println("Tiene que esperar a que haya 4 pasajeros para realizar alguna ruta");
                        if (alRuta1.size() == 4) {
                            System.out.println("Su ruta a completar tendrá destino en Portoalegre");
                            Conductores.Dequeue();
                            alRuta1.clear();
                        } else if (alRuta2.size() == 4) {
                            System.out.println("Su ruta a completar tendrá destino en Parque Colina");
                            Conductores.Dequeue();
                            alRuta2.clear();
                        } else if (alRuta3.size() == 4) {
                            System.out.println("Su ruta a completar tendrá destino en Mazuren");
                            Conductores.Dequeue();
                            alRuta3.clear();
                        } else if (alRuta4.size() == 4) {
                            System.out.println("Su ruta a completar tendrá destino en San Rafael");
                            Conductores.Dequeue();
                            alRuta4.clear();
                        }
                    } else {
                        System.out.println("Documento no registrado");
                    }
                    break;
                case 3:
                    System.out.println("Numero de veces a ingresar");
                    int numPrueba= leer.nextInt();
                    for(int i=1;i<=numPrueba;i++){
                        cliente cPrueba= new cliente("Camila", "Moreno", i, 98765);
                        clientes.put(String.valueOf(cPrueba.getDocumento()), cPrueba);
                    }
                    
                    break;
            }
        }
    }
}
