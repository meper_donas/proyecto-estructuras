/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import data.*;

public class AVL {

    class Node {

        int ht;
        int val;
        Node left;
        Node right;
        
        public Node(long val) {
            this.val = (int) val;
            this.ht = 0;
            this.left = null;
            this.right = null;
        }
    }

    public Node insert(Node root, long val) {
        Node nodo = new Node(val);

        if (root == null) {
            root = nodo;
        } else {
            root = avl(nodo, root);
        }
        return root;
    }

    public int height(Node root) {
        if (root == null) {
            return -1;
        } else {
            return 1 + Math.max(height(root.left), height(root.right));
        }

    }

    public Node RR(Node root) {
        Node temp = root.right;
        root.right = temp.left;
        temp.left = root;

        root.ht = height(root);
        temp.ht = height(temp);
        return temp;
    }

    public Node RL(Node root) {
        Node temp = root.left;
        root.left = temp.right;
        temp.right = root;

        root.ht = height(root);
        temp.ht = height(temp);
        return temp;
    }

    public Node DRRL(Node root) {
        root.left = RR(root.left);
        Node temp = RL(root);
        return temp;
    }

    public Node DRLR(Node root) {
        root.right = RL(root.right);
        Node temp = RR(root);
        return temp;
    }

    public Node avl(Node nodo, Node root) {
        Node Temp = root;
        if (nodo.val < root.val) {
            if (root.left == null) {
                root.left = nodo;
            } else {
                root.left = avl(nodo, root.left);
                if ((height(root.left) - height(root.right)) == 2) {
                    if (nodo.val < root.val) {
                        Temp = RL(root);
                    } else {
                        Temp = DRRL(root);
                    }
                }
            }
        } else if (nodo.val > root.val) {
            if (root.right == null) {
                root.right = nodo;
            } else {
                root.right = avl(nodo, root.right);  // branch right
                if ((height(root.right) - height(root.left)) == 2) {
                    if (nodo.val > root.val) {
                        Temp = RR(root);
                    } else {
                        Temp = DRLR(root);
                    }
                }
            }
        }
        Temp.ht = Math.max(height(Temp.left), height(Temp.right)) + 1;
        return Temp;
    }

    public void orden(Node root) {
        if (root == null) {
            return;
        }
        orden(root.left);
        //System.out.println(root.val);
        //arr[pos++]=root.val;
        orden(root.right);

    }

    public boolean contains(Node root, int val) {
        if (root == null) {
            return false;

        }
        if (val == root.val) {
            return true;

        }
        if (val > root.val) {
            return contains(root.right, val);

        }
        if (val < root.val) {
            return contains(root.left, val);

        }
        return false;
    }

}
